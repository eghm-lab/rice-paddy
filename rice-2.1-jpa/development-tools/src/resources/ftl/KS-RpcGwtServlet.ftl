/**
 * Copyright 2010 The Kuali Foundation Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may
 * obtain a copy of the License at
 *
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package org.kuali.student.core.atp.ui.server;

import org.kuali.student.r2.core.atp.service.${interface_class}Service;
import org.kuali.student.common.ui.server.gwt.BaseRpcGwtServletAbstract;
import org.kuali.student.core.atp.ui.client.service.${interface_class}RpcService;

/**
 * 
 * @author Kuali Student Team
 *
 */
public class ${interface_class}RpcGwtServlet extends BaseRpcGwtServletAbstract<${interface_class}Service> implements ${interface_class}RpcService {

	private static final long serialVersionUID = 1L;
	
}
