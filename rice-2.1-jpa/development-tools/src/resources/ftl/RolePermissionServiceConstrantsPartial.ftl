    /**
     * ${service} Roles and Permissions
     */
    public static final String ${service_upper}_MANAGER_ROLE_NAME = "${service} Manager";
    public static final String ${service_upper}_CONSUMER_ROLE_NAME = "${service} Consumer";

    public static final String CREATE_${service_upper}_PERMISSION = "Create ${service}";
    public static final String READ_${service_upper}_PERMISSION = "Read ${service}";
    public static final String UPDATE_${service_upper}_PERMISSION = "Update ${service}";
    public static final String DELETE_${service_upper}_PERMISSION = "Delete ${service}";

