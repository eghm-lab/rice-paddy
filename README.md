# rice-paddy
Kuali Rice code generation.

* rice-2.1-ojb - Old ImmutableJaxbGenerator from https://svn.kuali.org/repos/rice/sandbox/rice-2.1-ks-krms/ 
* rice-2.1-jpa - Latest ImmutableJaxbGenerator from https://svn.kuali.org/repos/rice/sandbox/rice-2.1-ks-krms/ 